# Update Ubuntu

bash <(curl -s https://gitlab.com/shukutils/update-ubuntu/-/raw/main/update_ubuntu.sh)

bash <(curl -s https://gitlab.com/shukutils/update-ubuntu/-/raw/main/update_ubuntu_fas.sh)


## Remove Ubuntu Pro prompts
bash <(curl -s https://gitlab.com/shukutils/update-ubuntu/-/raw/main/scripts/remove_ubuntu_pro_prompts.sh)



## Possible Slugs for Vorta:

### Only post-backup on positive, ignore failures:
if [ $returncode -eq 0 ]; then curl -m 10 --retry 5 "https://hc-ping.com/05ee6160-3c48-4109-8943-ebeb86d7717f"; fi

### Only post-backup on positive, but fail on failures:
curl -m 10 --retry 5 "https://hc-ping.com/05ee6160-3c48-4109-8943-ebeb86d7717f$([ $returncode -ne 0 ] && echo -n /fail)"

### Ignore trash and cache:
Version 1:
re:^(.*/)?\.?(cache|Cache|trash|Trash)(-1000)?(/.*)?$

/home/*/snap/spotify
/home/*/snap/steam