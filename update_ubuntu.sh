#!/bin/bash

# Install ansible-playbook
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ansible

sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install git

DIRECTORY="/tmp/$RANDOM"
git clone https://gitlab.com/shukutils/update-ubuntu.git $DIRECTORY
cd "$DIRECTORY"

sudo ansible-playbook ./setup_ubuntu_full.yml
