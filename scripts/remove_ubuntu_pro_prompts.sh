#!/bin/bash

echo "Disabling Ubuntu Pro prompts..."
echo "You need to enter your password, but it will not appear as ***** in the terminal, it will be invisible. Just enter it and press Enter."

echo "Disable the 'pro' command messaging"
sudo pro config set apt_news=false

# Variables for the file path
CONF_FILE="/etc/apt/apt.conf.d/20apt-esm-hook.conf"
BAK_FILE="/etc/apt/apt.conf.d/20apt-esm-hook.conf.bak"

echo "Renaming the $CONF_FILE file to $BAK_FILE and creating a zero-length file."

# Check if the original file exists
if [ -f "$CONF_FILE" ]; then
  # Rename the original file to .bak
  sudo mv "$CONF_FILE" "$BAK_FILE"
  echo "Renamed $CONF_FILE to $BAK_FILE"
else
  echo "$CONF_FILE does not exist."
fi

# Create a zero-length file with the same name
sudo touch "$CONF_FILE"
echo "Created a zero-length file at $CONF_FILE"

echo "Script execution completed successfully."
